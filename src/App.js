import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router-dom";

import "./App.css";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import LandingPage from "./components/Pages/LandingPage/LandingPage";
import CreatePollPage from "./components/Pages/CreatePollPage/CreatePollPage";
import SinglePollPage from "./components/Pages/SinglePollPage/SinglePollPage";
import ResultsPage from "./components/Pages/ResultsPage/ResultsPage";
import ErrorPage from "./components/Pages/ErrorPage/ErrorPage";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <PrivateRoute path="/poll" component={CreatePollPage} />
          <Route exact path="/notFound" component={ErrorPage} />
          <Route exact path="/:id" component={SinglePollPage} />
          <Route exact path="/results/:id" component={ResultsPage} />
          <Route component={ErrorPage} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
