import { action, observable } from "mobx";

class authStore {
  @observable isAuthenticated = "";
  @action setIsAuthenticated = bool => {
    this.isAuthenticated = bool;
  };
}

export default new authStore();
