import { action, observable } from "mobx";

class authStore {
  @observable newPollDivVisible = false;

  @action
  setIsVisible = bool => {
    this.newPollDivVisible = bool;
  };
}

export default new authStore();
