import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

import { BrowserRouter } from "react-router-dom";
import { Provider } from "mobx-react";
import authStore from "./stores/authStore";
import newPollStore from "./stores/newPollStore";

const Root = (
  <Provider authStore={authStore} newPollStore={newPollStore}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(Root, document.getElementById("root"));
registerServiceWorker();
