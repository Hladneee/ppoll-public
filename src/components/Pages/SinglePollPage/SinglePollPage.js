import React from 'react';

import "./SinglePollPage.css";
import PollHeader from "../../PollHeader/PollHeader";
import SinglePoll from "../../SinglePoll/SinglePoll";

const SinglePollPage = () => {
    return (
        <div className="SinglePollPageDiv">
            <div className="PollHeaderDiv">
                <PollHeader />
            </div>
            <div className="SinglePollDiv">
                <SinglePoll />
            </div>
        </div>
    );
}

export default SinglePollPage;
