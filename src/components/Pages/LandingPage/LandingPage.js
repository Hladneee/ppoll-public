import React, { Component } from "react";
import { withRouter } from "react-router";

import "./LandingPage.css";
import Login from "../../Login/Login";
import Ppoll from "../../Ppoll/Ppoll";
import Register from "../../Register/Register";

class LandingPage extends Component {
  render() {
    return (
      <div className="LandingPageDiv">
        <div className="left">
          <Ppoll />
        </div>
        <div className="right">
          <div className="rightTop">
            <Login />
          </div>
          <div className="rightBottom">
            <Register />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(LandingPage);
