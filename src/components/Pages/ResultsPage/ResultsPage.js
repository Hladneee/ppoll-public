import React from "react";

import "./ResultsPage.css";
import PollHeader from "../../PollHeader/PollHeader";
import Result from "../../Result/Result";

const ResultsPage = () => {
  return (
    <div className="SinglePollPageDiv">
      <div className="PollHeaderDiv">
        <PollHeader />
      </div>
      <div className="resultsDiv">
        <Result currentPoll={window.location.href.split("/")[4]}/>
      </div>
    </div>
  );
};

export default ResultsPage;
