import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import "./CreatePollPage.css";
import Header from "../../Header/Header";
import UserPolls from "../../UserPolls/UserPolls";
import NewPollForm from "../../NewPollForm/NewPollForm";


@inject("newPollStore")
@observer
class CreatePollPage extends Component {
  state = {
    showNewPoll: false
  };

  render() {
    return (
      <div className="CreatePollPage">
        <div className="headerDiv">
          <Header/>
        </div>
        <div className="userPollsDiv ">
          <UserPolls />
        </div>
        <div
          className={
            `newPollFormDiv ${this.props.newPollStore.newPollDivVisible ?  
            "visibleNewPollDiv" : "hiddenNewPollDiv"
          }`}
        >
          <NewPollForm/>
        </div>
      </div>
    );
  }
}
export default CreatePollPage;
