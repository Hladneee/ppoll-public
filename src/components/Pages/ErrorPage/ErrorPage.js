import React from "react";
import { Link, withRouter } from "react-router-dom";

import "./ErrorPage.css";

const ErrorPage = () => {
  return (
    <div className="errorPageDiv">
        <h2>404 Error, PPoll <span>not found!</span></h2>
        <Link to="/">Create your PPoll</Link>
    </div>
  );
};

export default withRouter(ErrorPage);
