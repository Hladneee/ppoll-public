import React, { Component } from "react";
import { inject, observer } from "mobx-react";

@inject("authStore")
@observer
class ArrayCounter extends Component {
  render() {
    const submitHandler = e => {
      e.preventDefault();
      const item = this.item.value;
      this.props.authStore.arrayAdd(item);
      this.item.value = "";
    };

    const { authStore } = this.props;

    return (
      <div>
        <h2>This is an array with {authStore.arrayLength} items</h2>
        <form onSubmit={submitHandler}>
          <input
            type="text"
            placeholder="Enter array item"
            ref={input => (this.item = input)}
          />
          <button>Add</button>
        </form>

        <ul>{authStore.array1.map((item, i) => <li key={i}>{item}</li>)}</ul>
      </div>
    );
  }
}

export default ArrayCounter;
