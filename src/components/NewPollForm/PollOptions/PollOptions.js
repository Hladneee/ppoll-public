import React, { Component } from "react";

import "./PollOptions.css";

class PollOptions extends Component {
  render() {
    let disableButton = false;
    const options = this.props.passedState.options.map((num, i) => {
      //disable Add Options button (!options)
      if (i >= 9) {
        disableButton = true;
      }
      return (
        <React.Fragment key={i}>
          <input
            className="option"
            type="text"
            required={i === 0 || i === 1}
            placeholder={`Option #${i + 1}..`}
            onChange={this.props.changeValueHandler.bind(this, i)}
          />
        </React.Fragment>
      );
    });
    return (
      <React.Fragment>
        <span className="newPollSpan" onClick={this.props.closeNewPollDiv}>
          X
        </span>
        {options}
        <button
          className="addOptionButton"
          onClick={this.props.addOptionHandler}
          disabled={disableButton}
        >
          Add Option
        </button>
      </React.Fragment>
    );
  }
}

export default PollOptions;
