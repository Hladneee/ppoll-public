import React, { Component } from "react";
import { inject } from "mobx-react";
import firebase from "firebase";

import "./NewPollForm.css";
import PollOptions from "./PollOptions/PollOptions";
import { database } from "../../firebase";

@inject("newPollStore")
class NewPollForm extends Component {
  state = {
    question: "",
    values: [[""], [""]],
    options: [1, 2],
    user: ""
  };

  submitHandler = e => {
    e.preventDefault();
    const Ppol = {
      question: this.state.question,
      values: this.state.values,
      options: this.state.options,
      user: this.state.user
    };
    database.ref("Polls").push(Ppol);
    this.closeNewPollDiv();
  };

  changeQuestionHandler = e => {
    const uid = firebase.auth().currentUser.uid;
    const questionAndUser = {
      question: e.target.value,
      user: uid
    };
    this.setState(questionAndUser);
  };

  changeValueHandler = (i, el) => {
    let values = [...this.state.values];
    values[i] = [el.target.value, 0];
    this.setState({ values });
  };

  addOptionHandler = e => {
    e.preventDefault();
    let options = [
      ...this.state.options,
      this.state.options[this.state.options.length - 1] + 1
    ];
    this.setState({ options });
  };

  closeNewPollDiv = () => {
    this.props.newPollStore.setIsVisible(false);
    this.setState({
      options: [1, 2],
      values: ["", ""]
    });
  };

  render() {
    return (
      <form className="newPollForm" onSubmit={this.submitHandler}>
        <h2>PPoll question:</h2>
        <input
          type="text"
          placeholder="Enter your question here.."
          required
          onChange={this.changeQuestionHandler}
        />
        <div className="inputDiv">
          <PollOptions
            passedState={this.state}
            changeValueHandler={this.changeValueHandler}
            addOptionHandler={this.addOptionHandler}
            closeNewPollDiv={this.closeNewPollDiv}
          />
          <input
            className="newPollFormSubmit"
            type="submit"
            disabled={
              !(
                this.state.question !== "" &&
                this.state.values[0] !== "" &&
                this.state.values[1] !== ""
              )
            }
          />
        </div>
      </form>
    );
  }
}
export default NewPollForm;
