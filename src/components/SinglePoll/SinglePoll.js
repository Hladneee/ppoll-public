import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import firebase from "firebase";

import "./SinglePoll.css";
import { database } from "../../firebase";

class SinglePoll extends Component {
  state = {
    pollData: {
      values: []
    },
    selected: null
  };

  componentDidMount() {
    const currentPoll = window.location.href.split("/")[3];

    //Check if poll exists

    const ref = firebase.database().ref(`Polls/${currentPoll}`);
    ref.once("value").then(
      function(snapshot) {
        const pollExist = snapshot.exists();
        if (!pollExist) {
          this.props.history.push("/notFound");
        }
      }.bind(this)
    );

    //pull data

    let query = database
      .ref("Polls")
      .orderByKey()
      .equalTo(currentPoll);
    query.on("child_added", this.gotData);
  }

  gotData = dataObj => {
    const pollObj = dataObj.val();
    this.setState({
      pollData: {
        values: pollObj.values,
        question: pollObj.question
      }
    });
  };

  changeHandler = e => {
    this.setState({ selected: e.target.value });
  };

  submitHandler = e => {
    e.preventDefault();
    const currentPoll = window.location.href.split("/")[3];
    const vote = this.state.selected;
    const ref = database.ref().child(`Polls/${currentPoll}/values/${vote}/1`);
    ref.transaction(function(newVoteValue) {
      return newVoteValue + 1;
    });
    this.props.history.push(`/results/${currentPoll}`);
  };

  render() {
    const options = this.state.pollData.values.map((val, id) => {
      val = val.slice(",")[0];
      return (
        <label className="optionLabel" key={id}>
          <input
            type="radio"
            name="option"
            id={id}
            value={id}
            onChange={this.changeHandler}
          />
          <div>{val}</div>
        </label>
      );
    });
    return (
      <div className="singlePoll">
        <h2>Select your vote option!</h2>
        <form className="optionsForm" onSubmit={this.submitHandler}>
          {this.state.pollData.question ? (
            <h2>{this.state.pollData.question}</h2>
          ) : (
            <h2>Loading...</h2>
          )}
          {options}
          <input
            className="submitVoteButton"
            type="submit"
            value="Vote!"
            disabled={!this.state.pollData.question}
          />
        </form>
        <Link className="viewResults" to="/">
          View Results
        </Link>
      </div>
    );
  }
}

export default withRouter(SinglePoll);
