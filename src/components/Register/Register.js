import React, { Component } from "react";
import firebaseApp from "../../firebase";

import "./Register.css";

const defaultState = {
  email: "",
  username: "",
  password: "",
  passwordRepeat: "",
  error: null
};

const byPropKey = (name, value) => () => ({
  [name]: value
});

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState };
  }

  submitHandler = e => {
    const { email, password } = this.state;
    e.preventDefault();

    firebaseApp
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(user => {
        this.setState(() => ({ ...defaultState }));
      })
      .catch(err => {
        this.setState(byPropKey("error", err));
      });
  };

  render() {
    const { email, username, password, passwordRepeat, error } = this.state;

    return (
      <div className="RegisterDiv">
        <h2>New user? Register here:</h2>
        <form className="registerForm" onSubmit={this.submitHandler}>
          <input
            type="text"
            value={email}
            placeholder="Email Address"
            required
            onChange={e => this.setState(byPropKey("email", e.target.value))}
          />
          <input
            type="text"
            value={username}
            placeholder="Username"
            required
            onChange={e => this.setState(byPropKey("username", e.target.value))}
          />
          <input
            type="password"
            value={password}
            placeholder="Password"
            required
            onChange={e => this.setState(byPropKey("password", e.target.value))}
          />
          <input
            type="password"
            value={passwordRepeat}
            placeholder="Confirm Password"
            required
            onChange={e =>
              this.setState(byPropKey("passwordRepeat", e.target.value))
            }
          />
          <input
            type="submit"
            placeholder="Submit"
            disabled={
              !(
                password === passwordRepeat &&
                password !== "" &&
                email !== "" &&
                username !== ""
              )
            }
          />
          {error && <p>{error.message}</p>}
        </form>
      </div>
    );
  }
}

export default Register;
