import React, { Component } from "react";
import { Route, Redirect } from "react-router";
import { inject } from "mobx-react";

@inject("authStore")
class PrivateRoute extends Component {
  render() {
    if (this.props.authStore.isAuthenticated)
      return <Route {...this.props}/>;
    else {
      return <Redirect to="/" />;
    }
  }
}

export default PrivateRoute;
