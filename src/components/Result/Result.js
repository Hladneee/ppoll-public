import React, { Component } from "react";
import firebase from "firebase";
import { HorizontalBar, Pie, defaults } from "react-chartjs-2";
import { withRouter } from "react-router-dom";

import "./Result.css";
import { database } from "../../firebase";

defaults.global.defaultFontColor = "#f2f2f2";

class Result extends Component {
  state = {
    pollData: {}
  };
  componentDidMount() {
    const currentPoll = this.props.currentPoll;

    //Check if poll exists
    const ref = firebase.database().ref(`Polls/${currentPoll}`);
    ref.once("value").then(
      function(snapshot) {
        const pollExist = snapshot.exists();
        if (!pollExist) {
          this.props.history.push("/notFound");
        }
      }.bind(this)
    );

    //pull data
    let query = database
      .ref("Polls")
      .orderByKey()
      .equalTo(currentPoll);
    query.on("child_added", this.gotData);
  }

  gotData = dataObj => {
    const pollObj = dataObj.val();
    const optionNames = [],
      optionValues = [];
    pollObj.values.forEach(element => {
      optionNames.push(element[0]);
      optionValues.push(element[1]);
      return optionNames, optionValues;
    });
    this.setState({
      question: pollObj.question,
      screenSize: window.innerWidth,
      pollData: {
        labels: optionNames,
        datasets: [
          {
            label: pollObj.question,
            data: optionValues,
            backgroundColor: [
              "rgba(36, 214, 228, 0.6)",
              "rgba(237, 57, 57, 0.6)",
              "rgba(49, 245, 80, 0.6)",
              "rgba(250, 216, 62, 0.8)",
              "rgba(164, 148, 255, 0.5)",
              "rgba(235, 252, 253, 0.8)",
              "rgba(203, 54, 255, 0.5)",
              "rgba(255, 127, 41, 0.65)",
              "rgba(82, 97, 255, 0.5)",
              "rgba(252, 83, 140, 0.7)"
            ],
            borderColor: [
              "rgba(36, 214, 228, 1)",
              "rgba(237, 57, 57, 1)",
              "rgba(49, 245, 80, 1)",
              "rgba(250, 216, 62, 1)",
              "rgba(164, 148, 255, 1)",
              "rgba(235, 252, 253, 1)",
              "rgba(203, 54, 255, 1)",
              "rgba(255, 127, 41, 1)",
              "rgba(82, 97, 255, 1)",
              "rgba(252, 83, 140, 1)"
            ],
            borderWidth: 2
          }
        ]
      }
    });
  };

  render() {
    return (
      <div className="resultDiv">
        <h2>{this.state.question}</h2>
        <div className="horizontalBarDiv">
          <HorizontalBar
            data={this.state.pollData}
            options={{
              maintainAspectRatio: false,
              legend: {
                display: false
              },
              scales: {
                xAxes: [
                  {
                    barThickness: this.state.screenSize > 1525 ? 50 : 20,
                    stacked: true,
                    gridLines: {
                      color: "rgba(235, 252, 253, 0.5)"
                    },
                    ticks: {
                      fontSize: 18
                    }
                  }
                ],

                yAxes: [
                  {
                    barThickness: this.state.screenSize > 1525 ? 50 : 20,
                    stacked: true,
                    gridLines: {
                      color: "transparent"
                    },
                    ticks: {
                      fontSize: 18
                    }
                  }
                ]
              }
            }}
          />
        </div>
        <div className="pieDiv">
          <Pie data={this.state.pollData} options={
            {
              maintainAspectRatio: false,
            }
          } />
        </div>
      </div>
    );
  }
}

export default withRouter(Result);
