import React, { Component } from "react";
import firebase from "firebase";
import { Link } from "react-router-dom";

import "./UserPolls.css";
import { database } from "../../firebase";

class UserPolls extends Component {
  state = {
    Polls: []
  };
  componentDidMount() {
    const uid = firebase.auth().currentUser.uid;
    database
      .ref("Polls")
      .orderByChild("user")
      .equalTo(uid)
      .on("value", this.gotData, this.errData);
  }

  gotData = dataObj => {
    const obj = dataObj.val();
    // log the obj from firebase console.log(obj);
    if (obj !== null) {
      const newState = Object.keys(obj).map(key => {
        return {
          name: key,
          question: obj[key].question
        };
      });
      this.updatePollsState(newState);
    }
  };

  updatePollsState = polls => {
    this.setState({
      Polls: polls
    });
  };

  errData = err => {
    console.log(`Error: ${err}`);
  };

  deleteHandler = pollId => {
    if (window.confirm("Are you sure you want to delete this poll?"))
      return firebase
        .database()
        .ref("Polls")
        .child(pollId)
        .remove();
    else return null;
  };

  render() {
    let pollNumber = 0;
    const allUserPolls = this.state.Polls.map((poll, i) => {
      pollNumber++;
      return (
        <React.Fragment key={i}>
          <div className="ppollLinkDiv">
            <Link className="ppollLink" to={`/${this.state.Polls[i].name}`}>
              {`Poll ${pollNumber}: ${this.state.Polls[i].question}`}
            </Link>
            <button
              className="ppollDeleteButton"
              onClick={this.deleteHandler.bind(this, this.state.Polls[i].name)}
            >
              Delete
            </button>
          </div>
        </React.Fragment>
      );
    });

    return (
      <div className="UserPollsDiv">
        {this.state.Polls ? <h2>Your created PPolls:</h2> : null}
        <div className="allPolls">{allUserPolls}</div>
      </div>
    );
  }
}

export default UserPolls;
