import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router";
import "./Header.css";

@inject("newPollStore")
@inject("authStore")
@observer
class Header extends Component {
  setIsVisible = () => {
    return this.props.newPollStore.setIsVisible(true);
  };

  logout = () => {
    this.props.authStore.setIsAuthenticated(false);
    this.props.history.push("/");
  }
  render() {
    return (
      <div className="header">
        <h2>PPoll</h2>
        <button onClick={this.setIsVisible}>New PPoll</button>
        <button onClick={this.logout}>Log Out</button>
      </div>
    );
  }
}

export default withRouter(Header);
