import React, { Component } from "react";
import { Redirect } from "react-router";
import firebase from "firebase";
import { inject, observer } from "mobx-react";

import "./Login.css";
import firebaseApp from "../../firebase";

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const defaultState = {
  email: "",
  password: "",
  error: null
};

@inject("authStore")
@observer
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState };
  }

  authHandler = authData => {
    return this.props.authStore.setIsAuthenticated(true);
  };

  authenticate = provider => {
    const authProvider = new firebase.auth[`${provider}AuthProvider`]();
    firebase
      .auth()
      .signInWithPopup(authProvider)
      .then(this.authHandler);
  };

  submitHandler = e => {
    const { email, password } = this.state;
    e.preventDefault();
    firebaseApp
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...defaultState }));
        this.props.authStore.setIsAuthenticated(true);
      })
      .catch(err => {
        this.setState(byPropKey("error", err));
        console.log(err);
      });
  };

  render() {
    const { email, password, error } = this.state;

    return (
      <div className="LoginDiv">
        <h2>Log In to make your Pretty poll:</h2>
        <button
          className="loginButton google"
          onClick={() => this.authenticate("Google")}
        />
        <button
          className="loginButton facebook"
          onClick={() => this.authenticate("Facebook")}
        />

        <form className="loginForm" onSubmit={this.submitHandler}>
          <input
            type="text"
            value={email}
            placeholder="Email Address"
            required
            onChange={e => this.setState(byPropKey("email", e.target.value))}
          />
          <input
            type="password"
            value={password}
            placeholder="Password"
            required
            onChange={e => this.setState(byPropKey("password", e.target.value))}
          />
          <input type="submit" placeholder="Log in" />
          {error && <p>{error.message}</p>}
        </form>
        {this.props.authStore.isAuthenticated ? (
          <Redirect push to="/poll" />
        ) : null}
      </div>
    );
  }
}

export default Login;
