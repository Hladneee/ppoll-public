import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./PollHeader.css";

class PollHeader extends Component {
  render() {
    return (
      <div className="header">
        <Link className="PpollLink" to="/">
          <h2>PPoll</h2>
        </Link>
      </div>
    );
  }
}

export default PollHeader;
