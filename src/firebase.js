import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBji-a6d9eMA-Ipq5MypupCX2X4VZo3ZHU",
  authDomain: "ppoll-a6ff6.firebaseapp.com",
  databaseURL: "https://ppoll-a6ff6.firebaseio.com"
});

const database = firebaseApp.database();

export { database };

export default firebaseApp;
